#
# Author:: Paulo Nahes (<phnahes@gmail.com>)
# License:: Apache License, Version 2.0
#

require_relative 'helper_databag'
require_relative 'helper_search'
require_relative 'helper_ssh'

module KnifeDeploy
	class Deploy < Chef::Knife

		include KnifeDeploy::HelperSearch
		include KnifeDeploy::HelperSSH
		include KnifeDeploy::HelperDatabag

		deps do
			require 'chef/api_client'
			require 'chef/data_bag'
			require 'chef/environment'
			require 'chef/exceptions'
			require 'chef/json_compat'
			require 'chef/knife'
			require 'chef/knife/search'
			require 'chef/search/query'
			require 'colorize'
			require 'json'
			require 'net/ssh'
			require 'net/ssh/multi'

			Chef::Knife.load_deps
			Chef::Knife::Ssh.load_deps
		end


		banner "knife deploy DATABAG BAG [ <Data Bag> <Data Bag Item> --env prod|homol --ticket 'ticket message' ]"
		category 'deploy'

		attr_reader :bag_name, :item_name

# TODO - Pensar na possibilidade de reativar isso
#		option :noedit,
#			:long => '--noedit',
#			:short => '-N',
#			:description => 'Upload using previous edited databag.'

		option :ticket,
			:long => '--ticket TICKET_STRING',
			:description => 'Ticket Number.',
			:required => true

		option :env,
			:long => '--env ENV_STRING',
			:description => 'Environment to apply changes.',
			:required => true
			

		# Definitions
		#

                def ask_edit_databag
                        answer = ui.ask_question("\nCorrect Changes? Y/N ", :default => "N").upcase
                        if answer == "Y"
                                return(true)
                        else
				ui.error "Exiting."
                                exit 1
                        end
                end

	        def ask_rollback
                        answer = ui.ask_question("Are you sure you would like to " + "Rollback ".bold.red + "to the previous version? Y/N ", :default => "N").upcase
                        if answer == "Y"
                                return(true)
                        else
                                return(false)
                        end
                end
	
		# Parse JSON File
		def parse(val)
			@parsed_val = JSON.parse(val)
		end
		
		# Get value TAG on Databag
		def get_value(json)
			jcont = parse(json)
			return jcont['tag']
		end

		# Chef Commands
		def cmd_stop
			return cmd = "sudo sed -i '/run-chef-client.sh/d' /var/spool/cron/crontabs/root; test -f /var/run/chef/cron-client.pid && sudo rm /var/run/chef/cron-client.pid && ps -ef | grep /usr/local/bin/run-chef-client.sh | grep -v grep | awk '{prin    t \$2}' | sudo xargs kill"
		end

		def cmd_run_chef
			cmd = "sudo /usr/bin/chef-client"
			return cmd
		end


		# Run Definition
		def run

			@bag_name, @item_name = @name_args
			
			unless @bag_name
				puts "\nYou need to specify a Data Bag!\n".bold
				show_usage
				exit 1
			end

			unless @item_name
				puts "\nYou need to specify a Bata Bag item name!\n".bold
				show_usage
				exit 1
			end

			Chef::Config[:solo]   = true
			@env 	= "#{config[:env]}"
			@ticket = "#{config[:ticket]}"

			validate_bags_path_exists

			@item_path_name = "#{bags_path}/#{@bag_name}/#{@item_name}.json"
			databag_file = @item_path_name

			@name_args = bag_name, @item_path_name

			# Nao Utilizado
			#
			if config[:noedit] == true
				not_edit_content
				tag = get_value(@f_read)
			else
				git = Chef::GitRepo.new(Chef::Config[:git_log])
				git.pull
	
				edit_content		
				tag = get_value @item_preformated
			end

			# Data Bag - Upload (Eg. knife data bag from file ...)
			#
			puts "\nGit control:".bold
			databag_from_file

			# Tags - Get server list and run the commands
			#
			puts "\nWorking on servers with TAG: #{tag.red} and Environment: #{@env.upcase.red}\n".bold


			list = server_list tag
			
			list.each do |server|
				puts "\nRunning commands on: #{server.green}".bold
				do_ssh("#{server}", "#{cmd_run_chef}")
				puts "Server #{server} was deployed.\n".bold
			end

     		end

	end
end

