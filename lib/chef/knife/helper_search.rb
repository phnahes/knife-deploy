#
# Author:: Paulo Nahes (<phnahes@gmail.com>)
# License:: Apache License, Version 2.0
#

module KnifeDeploy
	module HelperSearch

		require 'chef/search/query'
		require 'chef/knife/search'

		Chef::Knife.load_deps

		def server_list(tag)

			servers = Array.new

			#puts "Environment: #{@env.upcase.red}"
			query = "tags:#{tag} AND chef_environment:#{@env}"
			query_nodes = Chef::Search::Query.new

			query_nodes.search('node', query) do |node_item|
				servers << "#{node_item.fqdn}"
			end
			
			if servers.empty?
				puts "#{ui.color("No found Servers", :red)}"
				exit 1
			else
				servers.sort.each do |server_item|
					puts "Found Server: #{ui.color("#{server_item}", :green)}"
				end

			end

			return(servers.sort)

		end
	end
end



