#
# Author:: Paulo Nahes (<phnahes@gmail.com>)
# License:: Apache License, Version 2.0
#

module KnifeDeploy
	module HelperSSH

		require 'net/ssh'
		require 'net/ssh/multi'

		Chef::Knife.load_deps
		Chef::Knife::Ssh.load_deps


		def knife_ssh(host_list, command)
			ssh = Chef::Knife::Ssh.new
			ssh.name_args = [host_list, command]
			ssh.config[:ssh_user] = config[:ssh_user]
			ssh.config[:ssh_password] = config[:ssh_password]
			ssh.config[:ssh_port] = Chef::Config[:knife][:ssh_port] || config[:ssh_port]
			ssh.config[:identity_file] = config[:identity_file]
			ssh.config[:manual] = true
			ssh.config[:no_host_key_verify] = config[:no_host_key_verify]
			ssh
		end

		def do_ssh(host, command)	
			ssh = knife_ssh(host, command)
			begin
				ssh.run
			rescue Net::SSH::AuthenticationFailed
				unless config[:ssh_password]
					puts "Failed to authenticate #{config[:ssh_user]} - trying password auth"
					ssh = knife_ssh_with_password_auth(host_list, command)
					ssh.run
				end
			end
		end

	end
end
