#
# Author:: Paulo Nahes (<phnahes@gmail.com>)
# License:: Apache License, Version 2.0
#

module KnifeDeploy
	module HelperDatabag
	
		require 'chef/data_bag'
		require 'chef/data_bag_item'
		require 'chef/encrypted_data_bag_item'
		require 'chef/knife/core/object_loader'
		require 'chef/knife/data_bag_from_file'
		require 'json'

		Chef::Knife.load_deps

		attr_reader :bag_name, :item_name


		def normalize_item_paths(args)
			paths = Array.new
			args.each do |path|
				if File.directory?(path)
					paths.concat(Dir.glob(File.join(path, "*.json")))
				else
					paths << path
				end
			end
			paths
		end

		def data_bags_path
			@data_bag_path ||= "data_bags"
		end

		def validate_bags_path_exists
			unless File.directory? bags_path
				raise Chef::Exceptions::InvalidDataBagPath,
					"Configured data bag path '#{bags_path}' is invalid"
			end
		end

		def bags_path
			if config[:data_bag_path]
				Chef::Config[:data_bag_path] = config[:data_bag_path]
			end
			Chef::Config[:data_bag_path]
		end

		def bag_item_path
			File.expand_path File.join(bag_path, "#{item_name}.json")
		end

		def bag_path
			File.expand_path File.join(bags_path, bag_name)
		end

		private
		def bags
			#puts "bags_path #{bags_path}"
			Dir.entries(bags_path).select do |i|
				File.directory?(File.expand_path(File.join(bags_path, i))) && i != '.' && i != '..'
			end
		end

		# Used to get databag information from Chef-Server
		def existing_bag_item_content
			content = Chef::DataBagItem.load(bag_name, item_name).raw_data
			return content
		end

		def existing_bag_item_content_file_mode
			file_content = File.read(item_path_name_merge)
			content = parse(file_content)
			return content
		end
		
		# Used to build path and filename
		def item_path_name_merge
			return @item_path_name
		end

		# TODO - To complete develpment ...
		private
		def not_edit_content
				puts "DataBag File:\n#{item_path_name_merge.bold}"
				@f_read = File.read(item_path_name_merge)

				puts "DataBag Content:\n#{ui.color("#{@f_read}", :yellow)}"

				if !ask_edit_databag
					ui.error("Exiting.")
				end
					
				#json_parsed = parse(@f_read)
				#puts json_parsed
		end

		private
		def edit_content
			puts "Databag File:\n#{item_path_name_merge.bold.light_blue}\n"
			#puts "existing_bag_item_content_file_mode #{existing_bag_item_content_file_mode}"

			# Obtem valores antes de alterar
			#
			working_version_before = existing_bag_item_content_file_mode["#{@env}"]['version']

			# Abre editor com JSON parseado (existing_bag_item_content_file_mode = parseia json de item_path_name_merge)
			updated_content = edit_data existing_bag_item_content_file_mode

			@item_preformated = JSON.pretty_generate(updated_content) 
			puts "\nDataBag Content:\n#{ui.color("#{@item_preformated}", :yellow)}"

			working_version_after = updated_content["#{@env}"]['version']
			
			puts "\nEnvironment Informed: #{@env.upcase.bold.red}"
			puts "\nVersion Changes:\n"
			puts "- #{working_version_before}".bold.red
			puts "+ #{working_version_after}".bold.green
			puts "\n"
			
			# Confere versoes alteradas 
			#
			if working_version_after > working_version_before
				puts "Action: " + "Update Package".upcase.bold.red + "\n"
				@act = "UPDATE"

			elsif working_version_after < working_version_before
				puts "Action: " + "Rollback".upcase.bold.red
				@act = "ROLLBACK"
				if ask_rollback == false
					ui.error("\nExiting...")
					exit 1
				end

			else 
				puts "Action: No modifications or wrong envrionment."
				ui.error("\nExiting...")
				exit 0
			end
		
			# Pergunta se alteracao confere e salva no arquivo
			if ask_edit_databag
				persist_bag_item @item_preformated
			end
		end

		def persist_bag_item(item)
			File.open bag_item_path, 'w' do |f|
				f.write item
			end
		end

		def load_databag_item(bag, item_name)
			Chef::DataBagItem.load(bag, item_name)
		end

		private
		def databag_from_file(data_bag_names=nil)
			@message = "Action #{@act} :: Environment #{@env} :: Data_Bag #{@bag_name} :: Bag_Item #{@item_name} :: Ticket #{@ticket}"
			dbff = Chef::Knife::DataBagFromFile.new
			dbff.name_args = data_bag_names || @name_args
			dbff.config[:commit] = @message
			dbff.config[:editor] = config[:editor]
			dbff.config[:secret] = config[:secret]
			dbff.config[:secret_file] = config[:secret_file]
			dbff.run
		end
	end
end
