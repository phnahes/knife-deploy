# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)

require "knife-deploy/version"

Gem::Specification.new do |s|
  s.name        	= "knife-deploy"
  s.version     	= Knife::Deploy::VERSION
  s.authors     	= ["Paulo Nahes"]
  s.email       	= ["phnahes@gmail.com"]
  s.rubyforge_project 	= "knife-deploy"
  s.files         	= `git ls-files`.split("\n")
  s.test_files    	= `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   	= `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths 	= ["lib"]
  s.description   = %q{A knife plugin for make deploy easly and fastly}
  s.summary       = %q{Make deploy using data bag structure and all resources to automate all infra structure points}
  s.homepage      = "http://github.com/phnahes/knife-deploy"

  s.add_dependency('chef', '>= 11.5.4')
  s.add_dependency('git', '>= 1.2.5')
  s.add_dependency('colorize')
  s.add_dependency('json')

end
